import 'package:flutter/material.dart';

void main() => runApp(BallApp());


class BallApp extends StatelessWidget{
  Widget build(BuildContext context){
    return MaterialApp(
      title: 'Ball App',
      home: BallHomePage(),
    );
  }
}

class BallHomePage extends StatefulWidget{
  @override
  _StateBallHomePage createState() => _StateBallHomePage();
}

class _StateBallHomePage extends State<BallHomePage>{

  // double opacity = 0.0;
  bool _enabled = false;
  double leftMark = 50;

  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        title: Text('App Bar')
      ),
      body: _widgets()
    );
  }

  Widget _widgets(){
    return Stack(children: <Widget>[
          Positioned(
            top: 200,
            left: leftMark,
            child: Opacity(
              opacity: 1.0,
              child: Container(
                height: 100,
                width: 100,
                decoration: BoxDecoration(
                  color: Colors.yellow,
                  shape: BoxShape.circle,
                )
              )
            )
          ),
          Center(
            child: new Switch(
              value: _enabled,
              onChanged: (bool value) {
                setState(() {
                  _enabled = value;
                  leftMark = value ? 50 : 200;
                  // print(opacity);
                });
              }
            ),
          ),          
        ],
    );
  }
}